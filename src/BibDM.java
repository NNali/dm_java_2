import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
public class BibDM{


    public static Integer plus(Integer a, Integer b){
        return a+b;
    }



    public static Integer min(List<Integer> liste){
      if(liste.size()>0){
        Integer res = liste.get(0);
        for(Integer elem: liste){
          if(elem<res){
            res=elem;
          }
        }
        return res;
      }else{
        return null;
      }
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T elem: liste){
          if(elem.compareTo(valeur)<0){
            return false;
          }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> res = new ArrayList<T>();
      for(T elem:liste1){
        if(liste2.contains(elem) && !res.contains(elem)){
          res.add(elem);
        }
      }
      return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<String>();
        String mot = "";
        for(char lettre: texte.toCharArray()){
          if(!Character.toString(lettre).equals(" ")){
            mot+=Character.toString(lettre);
          }else{
            if(!mot.equals("")){
              res.add(mot);
              mot="";
            }
          }
        }
        if(!mot.equals("")){
          res.add(mot);
        }
        return res;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      if(texte.equals("")){
        return null;
      }
      List<String> liste_texte = decoupe(texte);
      Integer max = 0;
      String res = "";
      for(String elem: liste_texte){
        if(Collections.frequency(liste_texte, elem)>max){
          max=Collections.frequency(liste_texte, elem);
          res=elem;
        }else if(Collections.frequency(liste_texte, elem)==max){
          if(elem.compareTo(res)<0){
            res=elem;
            max=Collections.frequency(liste_texte, elem);
          }
        }
      }
      return res;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        Integer i = 0;
          for(char elem: chaine.toCharArray()){
            if(Character.toString(elem).equals("(")){
              i+=1;
            } else if(Character.toString(elem).equals(")")){
              if(i==0){
                return false;
              }
              i-=1;
            }
          }
        return i==0;
      }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      Integer i = 0;
      Integer j = 0;
        for(char elem: chaine.toCharArray()){
          if(Character.toString(elem).equals("(")){
            i+=1;
          } else if(Character.toString(elem).equals(")")){
            if(i==0){
              return false;
            }
            i-=1;
          } else if(Character.toString(elem).equals("[")){
            j+=1;
          } else if(Character.toString(elem).equals("]")){
            if(j==0){
              return false;
            }
            j-=1;
          }
        }
        if(chaine.contains("[)]") || chaine.contains("[(]")){
          return false;
        }
      return (i==0 && j==0);
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        Integer milieu = 0;
        Integer debut = 0;
        Integer fin = liste.size();
        boolean trouve = false;
        while(!trouve && ((fin - debut) > 1)){
          milieu = (debut + fin)/2;
          trouve = (liste.get(milieu)==valeur || liste.get(milieu-1)==valeur);
          if(liste.get(milieu)>valeur){
            fin = milieu;
          }else{
            debut = milieu;
          }
        }
        return trouve;
    }



}
